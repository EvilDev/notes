﻿using System.Windows;
using System.Windows.Input;
using Notes.Commands;

namespace Notes
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            _hotkey = new HotKey(Key.N, KeyModifier.Win, h => Command.Toggle.Execute(null));

            Window.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            _hotkey.Dispose();
            base.OnExit(e);
        }

        public static MainWindow Window = new MainWindow();
        private HotKey _hotkey;
    }
}