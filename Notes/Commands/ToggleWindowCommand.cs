﻿using System;
using System.Windows;
using System.Windows.Input;

namespace Notes.Commands
{
    public class ToggleWindowCommand : ICommand
    {

        public ToggleWindowCommand()
        {
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (App.Window.IsVisible)
            {
                App.Window.Hide();
            }
            else
            {
                App.Window.Show();
            }
        }

        public event EventHandler CanExecuteChanged;
    }

    public static class Command
    {
        public static ToggleWindowCommand Toggle
        {
            get
            {
                return new ToggleWindowCommand();
            }
        }
    }
}