﻿using System;
using System.IO;
using System.Windows.Controls;

namespace Notes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void TextBoxBase_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            var appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);

            var notesCache = Path.Combine(appData, "Notes", string.Format("{0:yyyy-MM-dd} notes.txt", DateTime.Now));

            if (!Directory.Exists(Path.GetDirectoryName(notesCache)))
                Directory.CreateDirectory(Path.GetDirectoryName(notesCache));

            if(File.Exists(notesCache)) File.Delete(notesCache);

            var text = ((TextBox) sender).Text;

            using (var stream = new FileStream(notesCache, FileMode.CreateNew))
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(text);
                }
            }
        }
    }
}